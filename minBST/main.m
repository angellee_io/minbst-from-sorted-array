//
//  main.m
//  minBST
//
//  Created by Angel Lee on 2016-02-11.
//  Copyright © 2016 Angel Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Node : NSObject

@property (nonatomic, strong) Node *left;
@property (nonatomic, strong) Node *right;
@property (nonatomic, assign) int value;

@end

@implementation Node

- (id)init {
    self = [super init];
    if (self) {

    }
    return self;
}

@end

Node *minBST(NSArray *arr, int start, int end);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
    }
    return 0;
}

// input array is sorted
Node *minBST(NSArray *arr, int start, int end) {
    if (start > end) {
        return nil;
    }
    
    int middleIndex = (start + end) /2;
    Node *mid = [Node new];
    mid.value = [arr[middleIndex] intValue];
    mid.left = minBST(arr, start, middleIndex-1);
    mid.right = minBST(arr, middleIndex+1, end);
    return mid;
}
